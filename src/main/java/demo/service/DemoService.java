/*
 * Copyright (c) 2018-2025, Sven Augustus (svenaugustus@outlook.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package demo.service;

import org.springframework.stereotype.Service;

import demo.common.service.IDemoService;
import demo.common.vo.DemoVO;

/**
 * 一个简单的Service样例
 * 
 * @author SvenAugustus(蔡政滦) e-mail: SvenAugustus@outlook.com
 * @version 2017年3月2日
 */
@Service
public class DemoService implements IDemoService {

	@Override
	public String sayHi(DemoVO vo) {
		return (vo == null) ? "Hi,who are you?" : ("Hi," + vo.getName());
	}
}
