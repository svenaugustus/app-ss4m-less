/*
 * Copyright (c) 2018-2025, Sven Augustus (svenaugustus@outlook.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package io.flysium.framework.jdbc.datasource.lookup;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import io.flysium.framework.jdbc.datasource.support.DynamicDataSourceContextHolder;

/**
 * 动态数据源
 * 
 * @author SvenAugustus(蔡政滦) e-mail: SvenAugustus@outlook.com
 * @version 1.0
 */
public class DynamicDataSource extends AbstractRoutingDataSource {

	/**
	 * 用户返回当且切换到的数据库
	 */
	@Override
	protected Object determineCurrentLookupKey() {
		String jndiName = DynamicDataSourceContextHolder.getDataSource();// DynamicDataSourceContextHolder有获取和设置当前数据库的方法get
																			// &
																			// put
		return jndiName;
	}

}
