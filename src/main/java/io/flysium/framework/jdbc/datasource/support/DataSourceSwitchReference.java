/*
 * Copyright (c) 2018-2025, Sven Augustus (svenaugustus@outlook.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package io.flysium.framework.jdbc.datasource.support;

import java.util.concurrent.ConcurrentHashMap;

/**
 * 全局数据源切换映射表
 * 
 * @author SvenAugustus(蔡政滦) e-mail: SvenAugustus@outlook.com
 * @version 1.0
 */
public final class DataSourceSwitchReference {

	/**
	 * 全局数据源切换映射表，采用ConcurrentHashMap保证读基本不需要锁，不影响高并发场景使用，而写操作仅在数据库宕机情况使用
	 */
	private static final ConcurrentHashMap<String, String> switchReference = new ConcurrentHashMap();

	private DataSourceSwitchReference() {
	}

	/**
	 * 设置全局数据源切换映射
	 * 
	 * @param name
	 *            数据源名称
	 * @param reserveJndiName
	 *            切换的目标数据源名称
	 */
	public static void putSwitchReference(String name, String reserveJndiName) {
		switchReference.put(name, reserveJndiName);
	}

	/**
	 * 获取全局数据源切换映射
	 * 
	 * @param name
	 * @return
	 */
	public static String getSwitchReference(String name) {
		return switchReference.get(name);
	}

}
