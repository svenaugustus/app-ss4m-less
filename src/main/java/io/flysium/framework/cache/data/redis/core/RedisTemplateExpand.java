/*
 * Copyright (c) 2018-2025, Sven Augustus (svenaugustus@outlook.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package io.flysium.framework.cache.data.redis.core;

import org.springframework.data.redis.core.RedisTemplate;

/**
 * Redis 操作模板扩展
 * 
 * @author SvenAugustus(蔡政滦) e-mail: SvenAugustus@outlook.com
 * @version 1.0
 */
public class RedisTemplateExpand<K, V> extends RedisTemplate<K, V> {

	private String defaultNamespace;

	/**
	 * 构造器
	 */
	public RedisTemplateExpand() {
		super();
	}

	/**
	 * 构造器
	 * 
	 * @param defaultNamespace
	 */
	public RedisTemplateExpand(String defaultNamespace) {
		super();
		this.defaultNamespace = defaultNamespace;
	}

	public String getDefaultNamespace() {
		return defaultNamespace;
	}

	public void setDefaultNamespace(String defaultNamespace) {
		this.defaultNamespace = defaultNamespace;
	}

}
